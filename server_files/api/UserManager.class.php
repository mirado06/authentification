<?php

require('User.class.php');
require('DataAccess.class.php');

class UserManager extends DataAccess {

    private static $_instance = null;

    public static function get_instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new UserManager();
        }
        return self::$_instance;
    }

    public function Insert(User $user) {
        $query = $this->_database->prepare('INSERT INTO users SET lastname = :lastname, firstname = :firstname, email = :email, login = :login, password = MD5(:password), right = :right');
        $query->bindValue(':lastname', $user->get_lastname());
        $query->bindValue(':firstname', $user->get_firstname());
        $query->bindValue(':email', $user->get_email());
        $query->bindValue(':login', $user->get_login());
        $query->bindValue(':password', $user->get_password());
        $query->bindValue(':right', $user->get_right());
        $query->execute();
        return $this->_database->lastInsertId();
    }

    public function Update(User $user, $id_user) {
        $query = $this->_database->prepare('UPDATE users SET lastname = :lastname, firstname = :firstname, email = :email, login = :login, password = MD5(:password), right = :right WHERE id_user = :id_user');
        $query->bindValue(':lastname', $user->get_lastname());
        $query->bindValue(':firstname', $user->get_firstname());
        $query->bindValue(':email', $user->get_email());
        $query->bindValue(':login', $user->get_login());
        $query->bindValue(':password', $user->get_password());
        $query->bindValue(':right', $user->get_right());
        $query->bindValue(':id_user', $id_user);
        $query->execute();
    }

    public function Remove($user) {
        if (is_int($user)) {
            $query = $this->_database->prepare('DELETE FROM users WHERE id_user = :id_user');
            $query->bindValue(':id_user', $user);
        } else {
            $query = $this->_database->prepare('DELETE FROM users WHERE lastname = :lastname AND firstname = :firstname AND email = :email AND login = :login AND password = MD5(:password) AND right = :right AND id_user = :id_user');
            $query->bindValue(':lastname', $user->get_lastname());
            $query->bindValue(':firstname', $user->get_firstname());
            $query->bindValue(':email', $user->get_email());
            $query->bindValue(':login', $user->get_login());
            $query->bindValue(':password', $user->get_password());
            $query->bindValue(':right', $user->get_right());
            $query->bindValue(':id_user', $user->get_id_user());
        }
        $query->execute();
    }

    public function GetList() {
        $query = $this->_database->query('SELECT id_user, lastname, firstname, email, login, users.right FROM users');
        $query->execute();
        $datas = $query->fetchAll(PDO::FETCH_ASSOC);
        $users = array();
        foreach ($datas as $data) {
            $users[] = new User($data);
        }
        return $datas;
    }

    public function Get($id_user) {
        $query = $this->_database->prepare("SELECT * FROM users WHERE id_user = :id_user");
        $query->bindValue(':id_user', $id_user);
        $query->execute();
        return new User($query->fetch(PDO::FETCH_ASSOC));
    }

    public function Authenticate($login, $password) {
        $query = $this->_database->prepare("SELECT id_user, lastname, firstname, email, login, users.right FROM users WHERE (email = :email OR login = :login) AND password = MD5(:password) LIMIT 1");
        $query->bindValue(':email', $login);
        $query->bindValue(':login', $login);
        $query->bindValue(':password', $password);
        $query->execute();
        $res = $query->fetchAll(PDO::FETCH_ASSOC);
        if (sizeof($res) === 1) {
//            return new User($res[0]);
            return $res[0];
        } else {
            return FALSE;
        }
    }

}

<?php

header('Access-Control-Allow-Origin: *');
require_once 'class_autoload.php';

function GetParam($param) {
    if (!isset($_REQUEST[$param])) {
        $response['message'] = 'Missing Parameter';
        echo json_encode($response);
        exit;
    }
    return $_REQUEST[$param];
}

$cmd = GetParam('cmd');
$response = NULL;
$UserManager = UserManager::get_instance();

switch ($cmd) {
    case 'LOGIN':
        $login = GetParam('login');
        $password = GetParam('password');
        $user = $UserManager->Authenticate($login, $password);
        $response['state'] = 'no';
        if ($user !== FALSE) {
            $response['state'] = "ok";
        }
        $response['user'] = $user;
        break;
    default:
        break;
}
echo json_encode($response);

<?php

class User {

    private $_id_user;
    private $_lastname;
    private $_firstname;
    private $_email;
    private $_login;
    private $_password;
    private $_right;

    public function __construct(array $datas = NULL) {
        if (isset($datas)) {
            $this->hydrate($datas);
        }
    }
    
    public function hydrate(array $datas) {
        foreach ($datas as $key => $value) {
            $methodName = 'set_' . $key;
            if (method_exists($this, $methodName)) {
                $this->$methodName($value);
            }
        }
    }
    
    public function get_id_user() {
        return $this->_id_user;
    }

    public function get_lastname() {
        return $this->_lastname;
    }

    public function get_firstname() {
        return $this->_firstname;
    }

    public function get_email() {
        return $this->_email;
    }

    public function get_login() {
        return $this->_login;
    }

    public function get_password() {
        return $this->_password;
    }

    public function get_right() {
        return $this->_right;
    }

    public function set_id_user($_id_user) {
        $this->_id_user = $_id_user;
    }

    public function set_lastname($_lastname) {
        $this->_lastname = $_lastname;
    }

    public function set_firstname($_firstname) {
        $this->_firstname = $_firstname;
    }

    public function set_email($_email) {
        $this->_email = $_email;
    }

    public function set_login($_login) {
        $this->_login = $_login;
    }

    public function set_password($_password) {
        $this->_password = $_password;
    }

    public function set_right($_right) {
        $this->_right = $_right;
    }

}

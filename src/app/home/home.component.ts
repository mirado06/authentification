import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
//import {LoginService} from './../login/login.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styles: []
})
export class HomeComponent implements OnInit {

    constructor(
        private route: ActivatedRoute,
        private router: Router,
//        private loginservice: LoginService
        ) {}

    ngOnInit() {
        if (!localStorage.getItem('user')) {
            this.router.navigate(['/login'], {});
        }
    }
    logout() {
        localStorage.removeItem("user");
        this.router.navigate(['/login'], {});
    }
//    logout() {
//        this.loginservice.logout();
//        this.router.navigate(['/login'], {});
//    }

}

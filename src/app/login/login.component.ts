import {Component, Inject, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {LoginService} from './login.service';
import {User} from './../user';

import 'rxjs/add/operator/switchMap';
@Component({
    providers: [LoginService],
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
    public msg = '';
    usr: User;

    form: FormGroup;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private loginservice: LoginService,
        @Inject(FormBuilder) fb: FormBuilder
    ) {
        this.form = fb.group({
            login: ['', Validators.required],
            password: ['', Validators.required],
        });
    }

    ngOnInit() {
        if (localStorage.getItem('user')) {
            this.router.navigate(['/home'], {});
        }
    }
    login() {
        this.loginservice.login(this.form.get('login').value, this.form.get('password').value)
            .subscribe(res => {
                if (res['state'] == "ok") {
                    this.usr = res['user'];
//                    this.msg = "Connecté en tant que : " + this.usr.firstname;
                    localStorage.setItem('user', JSON.stringify(res['user']));
                    this.router.navigate(['/home'], {});
                }
                else {
                    this.msg = 'Identifiants invalides';
                }
            });
    }
}
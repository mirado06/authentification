import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import {User} from './../user';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {
    constructor(
        private http: Http) {}

    logout() {
        localStorage.removeItem("user");
    }

    login(login:string, password:string) {
        return this.http
            .get(`http://localhost/api/server.php?cmd=LOGIN&login=${login}&password=${password}`)
            .map(resp => {
                resp.json();
                return resp.json();
            });
    }
}



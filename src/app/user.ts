export class User {

    constructor(
        public id_user: number,
        public lastname: string,
        public firstname: string,
        public email: string,
        public login: string,
        public password: string,
        public right: number
    ) {}

}
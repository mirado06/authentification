import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {EssaiComponent} from './essai/essai.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';

const routes: Routes = [
    {path: '', redirectTo: '/essai', pathMatch: 'full'},
    {path: 'essai', component: EssaiComponent},
    {path: 'home', component: HomeComponent},
    {path: 'login', component: LoginComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
